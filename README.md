# Zadanie 2

Stworzyć lokalne repozytorium git o nazwie zad2.
W repozytorium proszę umieścić plik instrukcja.md. 
Plik ten powinien zawierać  wersję rozdziału Lekcja 1 (tejże instrukcji) ale w formacie Markdown.Postępy prac rejestrować jako kolejne commity. 

Podczas oceny będzie brana pod uwagę liczba commitów jak i jasność przypisanych im komunikatów.
Następnie repozytorium umieścić na serwerze gitlab w projekcie o nazwie zad2. 
Do projektu jako użytkownika dodać usera mniewins66.
Jako wynik pracy przesłać link do projektu na serwerze gitlab lub opcjonalnie spakowane repozytorium lokalne wysłać e-mailem do prowadzącego (lub w zasobach kursu)

